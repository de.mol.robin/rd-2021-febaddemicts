package com.inetum.realdolmen.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface Password {
    String message() default "Password invalid. Rules: at least 8 characters, at least 1 digit, at least 1 non-alphanumerical symbol.";
    Class<?>[] groups() default {}; // to control the order of validation
    Class<? extends Payload>[] payload() default {}; // to associate meta-data
}
