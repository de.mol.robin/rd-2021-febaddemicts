package com.inetum.realdolmen.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        if (value.length() < 8) {
            // at least 8 characters are required
            return false;
        }
        if (value.chars().noneMatch(Character::isDigit)) {
            // one digit is required
            return false;
        }
        if (value.chars().allMatch(Character::isLetterOrDigit)) {
            // one non-alphanumeric character is required
            return false;
        }
        return true;
    }
}
