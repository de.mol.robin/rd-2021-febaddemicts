package com.inetum.realdolmen.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DeliveryAfterPurchaseValidator.class)
public @interface DeliveryAfterPurchase {
    String message() default "Delivery date must be >= purchase date";
    Class<?>[] groups() default {}; // to control the order of validation
    Class<? extends Payload>[] payload() default {}; // to associate meta-data
}
