package com.inetum.realdolmen.validation;

import com.inetum.realdolmen.entities.Order;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DeliveryAfterPurchaseValidator implements ConstraintValidator<DeliveryAfterPurchase, Order> {
    @Override
    public boolean isValid(Order value, ConstraintValidatorContext context) {
        if (value == null || value.getDeliveryDate() == null || value.getPurchaseDate() == null) {
            return true;
        }
        return value.getDeliveryDate().isAfter(value.getPurchaseDate())
                || value.getDeliveryDate().isEqual(value.getPurchaseDate());
    }
}
