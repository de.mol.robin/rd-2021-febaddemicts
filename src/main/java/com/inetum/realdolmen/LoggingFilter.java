package com.inetum.realdolmen;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebFilter(urlPatterns = "*")
public class LoggingFilter extends HttpFilter {

    @Inject
    private Logger logger;

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        System.out.println("In filter before servlet call");
        long before = System.nanoTime();
        chain.doFilter(req, res);
        long after = System.nanoTime();
        logger.info("Call took " + (after - before) + "ns");
    }
}
