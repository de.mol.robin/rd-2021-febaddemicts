package com.inetum.realdolmen.resources;

import com.inetum.realdolmen.beans.OrderDAO;
import com.inetum.realdolmen.exceptions.EntityNotFound;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("orders")
public class OrderResource {

    @Inject
    private OrderDAO orderDAO;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<OrderDTO> getAll() {
        return orderDAO.getAll()
                       .stream()
                       .map(OrderDTO::fromOrder)
                       .collect(Collectors.toList());
    }

    @PUT
    @Path("{orderId}")
    @Secured
    public void markAsPaid(@PathParam("orderId") long id) throws EntityNotFound {
        orderDAO.markAsPaid(id);
    }

}
