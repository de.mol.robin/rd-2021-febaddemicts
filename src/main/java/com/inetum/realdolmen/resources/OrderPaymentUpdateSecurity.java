package com.inetum.realdolmen.resources;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Secured
public class OrderPaymentUpdateSecurity implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String auth = requestContext.getHeaderString("Authentication");
        if (!"ThisIsASecret".equals(auth)) {
            requestContext.abortWith(Response.status(401).build());
        }
    }
}
