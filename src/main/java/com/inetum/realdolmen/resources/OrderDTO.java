package com.inetum.realdolmen.resources;

import com.inetum.realdolmen.entities.Order;

public class OrderDTO {
    private final long id;
    private final boolean paid;

    private OrderDTO(long id, boolean paid) {
        this.id = id;
        this.paid = paid;
    }

    public static OrderDTO fromOrder(Order o) {
        return new OrderDTO(o.getId(), o.isPaid());
    }

    public long getId() {
        return id;
    }

    public boolean isPaid() {
        return paid;
    }
}
