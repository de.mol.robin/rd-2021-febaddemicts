package com.inetum.realdolmen;

import com.inetum.realdolmen.beans.PeopleDAO;
import com.inetum.realdolmen.entities.Person;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/admins")
public class AdminServlet extends HttpServlet {

    @Inject
    private PeopleDAO peopleDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var admins = peopleDAO.getAllAdmins()
                              .stream()
                              .map(Person::toJson)
                              .collect(Collectors.joining(",", "[", "]"));
        resp.getWriter()
            .println(admins);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String password = req.getParameter("password");
        peopleDAO.createNewAdmin(email, firstName, lastName, password);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        String password = req.getParameter("password");
        peopleDAO.updatePasswordForUser(id, password);
    }
}
