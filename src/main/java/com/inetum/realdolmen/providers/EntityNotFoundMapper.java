package com.inetum.realdolmen.providers;

import com.inetum.realdolmen.exceptions.EntityNotFound;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundMapper implements ExceptionMapper<EntityNotFound> {
    @Override
    public Response toResponse(EntityNotFound exception) {
        return Response.status(404)
                       .entity(exception.getMessage())
                       .build();
    }
}
