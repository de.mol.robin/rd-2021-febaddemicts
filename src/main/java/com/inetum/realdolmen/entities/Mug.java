package com.inetum.realdolmen.entities;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "mugs")
public class Mug extends Merch {
    public enum MugSize {
        small, medium, large, XL
    }
    @Column(name = "mugsize")
    @Enumerated(EnumType.STRING)
    private MugSize mugSize;

    public Mug() { }

    public MugSize getMugSize() {
        return mugSize;
    }

    public void setMugSize(MugSize mugSize) {
        this.mugSize = mugSize;
    }

    @Override
    public String toJson() {
        StringJoiner sj = new StringJoiner(",", "{", "}");
        sj.add("\"name\":\"" + getName() + "\"");
        sj.add("\"description\":\"" + getDescription() + "\"");
        sj.add("\"price\":" + getPrice());
        sj.add("\"mugSize\":\"" + mugSize + "\"");
        return sj.toString();
    }
}
