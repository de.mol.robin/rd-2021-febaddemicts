package com.inetum.realdolmen.entities;

import com.inetum.realdolmen.validation.Password;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

@Entity
@Table(name = "persons")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PersonGen")
    @SequenceGenerator(name = "PersonGen", sequenceName = "persons_id_seq", allocationSize = 1)
    private Long id;

    @Email
    @NotNull
    private String email;
    @Password
    @NotNull
    private String password;
    @Column(name = "first_name")
    @NotBlank
    private String firstName;
    @Column(name = "last_name")
    @NotBlank
    private String lastName;

    @ElementCollection
    @CollectionTable(name = "groups", joinColumns = @JoinColumn(name = "person_id"))
    @Column(name = "name")
    private Set<@NotBlank String> groups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<String> getGroups() {
        return groups;
    }

    public void setGroups(Set<String> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toJson() {
        StringJoiner sj = new StringJoiner(",", "{", "}");
        sj.add("\"id\":" + id);
        sj.add("\"email\":\"" + email + "\"");
        sj.add("\"firstName\":\"" + firstName + "\"");
        sj.add("\"lastName\":\"" + lastName + "\"");
        sj.add("\"password\":\"" + password + "\"");
        return sj.toString();
    }
}
