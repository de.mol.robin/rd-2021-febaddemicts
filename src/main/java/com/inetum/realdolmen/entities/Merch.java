package com.inetum.realdolmen.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

@Entity
@Table(name = "merch")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Merch {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MerchGen")
    @SequenceGenerator(name = "MerchGen", sequenceName = "merch_id_seq", initialValue = 1, allocationSize = 1)
    private Long id;
    @NotBlank
    private String name;
    private String description;
    @Positive
    @NotNull
    private Float price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturer;

    public Merch() { }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Merch merch = (Merch) o;
        return Objects.equals(id, merch.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public abstract String toJson();
}
