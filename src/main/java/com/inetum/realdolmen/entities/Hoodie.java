package com.inetum.realdolmen.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.StringJoiner;

@Entity
@Table(name = "hoodies")
public class Hoodie extends Merch {
    private String size;
    private String color;

    public Hoodie() {
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toJson() {
        StringJoiner sj = new StringJoiner(",", "{", "}");
        sj.add("\"name\":\"" + getName() + "\"");
        sj.add("\"description\":\"" + getDescription() + "\"");
        sj.add("\"price\":" + getPrice());
        sj.add("\"size\":\"" + size + "\"");
        sj.add("\"color\":\"" + color + "\"");
        return sj.toString();
    }
}
