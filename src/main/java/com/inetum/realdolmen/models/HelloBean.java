package com.inetum.realdolmen.models;

import javax.enterprise.inject.Model;
import java.time.LocalDateTime;

@Model
public class HelloBean {

    private String name = "Robin De Mol";
    private LocalDateTime now = LocalDateTime.now();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getNow() {
        return now;
    }

    public void setNow(LocalDateTime now) {
        this.now = now;
    }
}
