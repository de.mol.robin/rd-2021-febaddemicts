package com.inetum.realdolmen.models;

import com.inetum.realdolmen.beans.MerchDAO;
import com.inetum.realdolmen.entities.Mug;

import javax.ejb.EJB;
import javax.enterprise.inject.Model;

@Model
public class MugBean {
    private Mug mug;

    @EJB
    private MerchDAO merchDAO;

    public void loadMugById(Long id) {
        this.mug = merchDAO.findMugById(id);
    }

    public Mug getMug() {
        return mug;
    }
}
