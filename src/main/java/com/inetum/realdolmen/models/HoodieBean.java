package com.inetum.realdolmen.models;

import com.inetum.realdolmen.beans.MerchDAO;
import com.inetum.realdolmen.entities.Hoodie;

import javax.ejb.EJB;
import javax.enterprise.inject.Model;

@Model
public class HoodieBean {
    private Hoodie hoodie;

    @EJB
    private MerchDAO merchDAO;

    public void loadHoodieById(Long id) {
        this.hoodie = merchDAO.findHoodieById(id);
    }

    public Hoodie getHoodie() {
        return hoodie;
    }
}
