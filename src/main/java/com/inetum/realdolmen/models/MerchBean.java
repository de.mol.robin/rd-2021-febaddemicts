package com.inetum.realdolmen.models;

import com.inetum.realdolmen.beans.MerchDAO;
import com.inetum.realdolmen.entities.Merch;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Model;
import java.util.List;

@Model
public class MerchBean {

    @EJB
    private MerchDAO merchDAO;

    private List<Merch> merch;

    private String filter;

    @PostConstruct
    public void applyFilter() {
        merch = merchDAO.getMatchingByName(filter);
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<Merch> getMerch() {
        return merch;
    }
}
