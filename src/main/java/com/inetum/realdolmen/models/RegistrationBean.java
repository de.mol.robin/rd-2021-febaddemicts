package com.inetum.realdolmen.models;

import com.inetum.realdolmen.beans.PeopleDAO;
import com.inetum.realdolmen.validation.Password;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Model
public class RegistrationBean {
    @Email
    @NotNull
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Password
    @NotNull
    private String password;

    @Inject
    private PeopleDAO peopleDAO;

    public String submit() {
        try {
            peopleDAO.createNewUser(email, firstName, lastName, password);
            return "people?faces-redirect=true";
        } catch (Exception e) {
            return null;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
