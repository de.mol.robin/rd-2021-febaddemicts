package com.inetum.realdolmen.models;

import com.inetum.realdolmen.beans.PeopleDAO;
import com.inetum.realdolmen.entities.Person;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;

@Model
public class PeopleBean {

    private List<Person> people;

    @Inject
    private PeopleDAO peopleDAO;

    @PostConstruct
    public void loadPeople() {
        this.people = peopleDAO.getAll();
    }

    public List<Person> getPeople() {
        return people;
    }
}
