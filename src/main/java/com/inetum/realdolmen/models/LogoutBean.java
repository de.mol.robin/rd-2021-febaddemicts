package com.inetum.realdolmen.models;

import javax.enterprise.inject.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Model
public class LogoutBean {

    public String logout(HttpServletRequest request) {
        try {
            request.logout();
            request.getSession().invalidate();
            return "/store.xhtml?faces-redirect=true";
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return null;
    }
}
