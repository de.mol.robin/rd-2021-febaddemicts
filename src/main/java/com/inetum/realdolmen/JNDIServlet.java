package com.inetum.realdolmen;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet("/jndi")
public class JNDIServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String ns = Optional.ofNullable(req.getParameter("ns"))
                            .orElse("java:");
        List<String> contexts = new ArrayList<>();
        contexts.add(ns);
        var out = resp.getWriter();
        try {
            var jndi = new InitialContext();
            while (!contexts.isEmpty()) {
                var ctx = contexts.remove(0);
                try {
                    var bindings = jndi.listBindings(ctx);
                    while (bindings.hasMore()) {
                        Binding next = bindings.next();
                        String name = ctx + "/" + next.getName();
                        if (next.getObject() instanceof Context) {
                            contexts.add(name);
                        } else {
                            System.out.println(name + "(" + next + ")");
                            out.println(name);
                        }
                    }
                } catch (NamingException e) {
                    System.out.println("Naming exception for '" + ctx + "'");
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
