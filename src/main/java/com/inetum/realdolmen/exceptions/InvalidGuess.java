package com.inetum.realdolmen.exceptions;

public class InvalidGuess extends Exception {
    public InvalidGuess() {
    }

    public InvalidGuess(String message) {
        super(message);
    }

    public InvalidGuess(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidGuess(Throwable cause) {
        super(cause);
    }

    public InvalidGuess(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
