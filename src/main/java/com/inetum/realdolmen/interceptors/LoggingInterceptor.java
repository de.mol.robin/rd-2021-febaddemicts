package com.inetum.realdolmen.interceptors;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Interceptor
@Logged
public class LoggingInterceptor {

    @Inject
    private Logger log;

    @AroundInvoke
    public Object logInfoOnInvocation(InvocationContext ic) throws Exception {
        var audit = String.join(" ", ic.getTarget()
                                       .toString(), ".", ic.getMethod()
                                                           .getName(), "is being invoked with params", Arrays.stream(ic.getParameters())
                                                                                                             .map(Object::toString)
                                                                                                             .collect(Collectors.joining(", ")));
        log.info(audit);
        try {
            return ic.proceed();
        } catch (Exception e) {
            throw e;
        }
    }
}
