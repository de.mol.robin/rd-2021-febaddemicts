package com.inetum.realdolmen;

import com.inetum.realdolmen.beans.NameCleanerBean;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GreeterServlet extends HttpServlet {

    @EJB
    NameCleanerBean ncb;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cleanedName = ncb.cleanName(req.getParameter("name"));
        resp.getWriter()
            .println("Hello, " + cleanedName + "!");
    }
}
