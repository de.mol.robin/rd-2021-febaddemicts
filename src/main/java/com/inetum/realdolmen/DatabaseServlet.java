package com.inetum.realdolmen;

import com.inetum.realdolmen.beans.MerchDAO;
import com.inetum.realdolmen.entities.Merch;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/db")
public class DatabaseServlet extends HttpServlet {

    @EJB
    private MerchDAO merchDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var merch = merchDAO.getAll()
                            .stream()
                            .map(Merch::toJson)
                            .collect(Collectors.joining(",", "[", "]"));
        resp.getWriter()
            .println(merch);
    }
}
