package com.inetum.realdolmen.soap;

import com.inetum.realdolmen.beans.PeopleDAO;
import com.inetum.realdolmen.entities.Person;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class PersonWS {

    @Inject
    private PeopleDAO peopleDAO;

    @WebMethod
    public List<Person> getAll(){
        return peopleDAO.getAll();
    }

    @WebMethod
    public Person getById(@WebParam long id) {
        return peopleDAO.getById(id);
    }
}
