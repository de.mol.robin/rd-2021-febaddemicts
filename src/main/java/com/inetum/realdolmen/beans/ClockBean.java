package com.inetum.realdolmen.beans;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.logging.Logger;

@Singleton
@Startup
public class ClockBean {

    @Inject
    private Logger log;

    @Schedule(year = "*", month = "*", dayOfMonth = "*", hour = "*", minute = "50")
    public void timeForABreak() {
        log.warning("ClockBean: it's time for a break!");
    }

}
