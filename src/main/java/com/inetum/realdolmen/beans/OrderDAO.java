package com.inetum.realdolmen.beans;

import com.inetum.realdolmen.entities.Order;
import com.inetum.realdolmen.exceptions.EntityNotFound;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class OrderDAO {
    @PersistenceContext(unitName = "InetumPU")
    private EntityManager entityManager;

    public List<Order> getAll() {
        return entityManager.createQuery("select o from Order o", Order.class)
                            .getResultList();
    }

    @Transactional
    public void markAsPaid(long id) throws EntityNotFound {
        Optional.ofNullable(entityManager.find(Order.class, id))
                .orElseThrow(() -> new EntityNotFound("Could not find order with id '" + id + "'"))
                .setPaid(true);
    }
}
