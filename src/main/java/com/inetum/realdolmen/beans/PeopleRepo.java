package com.inetum.realdolmen.beans;

import com.inetum.realdolmen.entities.Person;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class PeopleRepo {

    @PersistenceContext
    private EntityManager entityManager;

    public long countPeopleWithEmail(String email) {
//        return entityManager.createQuery("select count(p) from Person p where p.email = ?1", Long.class)
//                            .setParameter(1, email)
//                            .getSingleResult();
        Person p = entityManager.createQuery("select p from Person p where p.email = ?1", Person.class)
                                .setFlushMode(FlushModeType.COMMIT)
                                .setParameter(1, email)
                                .getSingleResult();
        if (p != null) {
            entityManager.detach(p);
            return 1;
        }
        return 0;
    }
}
