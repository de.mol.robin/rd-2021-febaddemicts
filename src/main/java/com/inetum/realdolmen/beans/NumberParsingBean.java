package com.inetum.realdolmen.beans;

import com.inetum.realdolmen.exceptions.InvalidGuess;

import javax.ejb.Stateless;

@Stateless
public class NumberParsingBean {

    public int parseAsNumber(String input) throws InvalidGuess {
        if (input == null) {
            throw new InvalidGuess("You must supply a value to guess");
        }
        int guess;
        try {
            guess = Integer.parseInt(input);
        } catch (Exception e) {
            throw new InvalidGuess("Supplied guess must be an integer");
        }
        if (guess < 0 || guess > 100) {
            throw new InvalidGuess("Guess must be between 0 and 100");
        }
        return guess;
    }
}
