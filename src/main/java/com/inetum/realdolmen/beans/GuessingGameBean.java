package com.inetum.realdolmen.beans;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import java.util.Random;

@Stateful
public class GuessingGameBean {

    private int guesses;
    private int number;

    public String makeGuess(int g) {
        if (guesses <= 0) {
            return "You have no guesses left, please reset the game first!";
        }
        guesses--;
        String outcome;
        if (g == number) {
            outcome = "Correct!!!";
            initializeGame();
        } else if (g < number) {
            outcome = "Higher!";
        } else {
            outcome = "Lower!";
        }
        if (guesses == 0) {
            outcome = "Game over! The number was " + this.number;
            initializeGame();
        }
        return outcome;
    }

    public int getGuesses() {
        return guesses;
    }

    @PostConstruct
    public void initializeGame() {
        this.guesses = 5;
        this.number = new Random().nextInt(100);
        System.out.println("A new guessing game has been initialized with number " + this.number);
    }
}
