package com.inetum.realdolmen.beans;

import com.inetum.realdolmen.entities.Person;
import com.inetum.realdolmen.interceptors.Logged;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class PeopleDAO {

    @PersistenceContext(unitName = "InetumPU")
    private EntityManager entityManager;

    public List<Person> getAllAdmins() {
        return entityManager.createQuery("select p from Person p JOIN p.groups g WHERE g = ?1", Person.class)
                            .setParameter(1, "Admin")
                            .getResultList();
    }

    public List<Person> getAll() {
        return entityManager.createQuery("select p from Person p left JOIN fetch p.groups g", Person.class)
                            .getResultList();
    }

    @Logged
    @Transactional
    public void createNewUser(String email, String firstName, String lastName, String password) {
        var p = new Person();
        p.setEmail(email);
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setPassword(password);
        p.setGroups(Set.of("User"));
        entityManager.persist(p);
    }

    @Transactional
    public void createNewAdmin(String email, String firstName, String lastName, String password) {
        var p = new Person();
        p.setEmail(email);
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setPassword(password);
        p.setGroups(Set.of("Admin"));
        entityManager.persist(p);
    }

    @Transactional
    public void updatePasswordForUser(Long id, String password) {
        entityManager.find(Person.class, id)
                     .setPassword(password);
    }

    public Person getById(long id) {
        return entityManager.createQuery("select p from Person p join fetch p.groups where p.id = ?1", Person.class)
                            .setParameter(1, id)
                            .getSingleResult();
//        return entityManager.find(Person.class, id);
    }
}
