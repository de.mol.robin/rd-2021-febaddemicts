package com.inetum.realdolmen.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import java.util.Optional;

@Stateless
public class NameCleanerBean {

    @PostConstruct
    public void setup() {
        System.out.println("Hello from " + Thread.currentThread().getName());
    }

    public String cleanName(String name) {
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return Optional.ofNullable(name)
                       .map(s -> s.substring(0, 1)
                                  .toUpperCase() + s.substring(1)
                                                    .toLowerCase())
                       .orElse("World");
    }

    @PreDestroy
    public void cleanup() {
        System.out.println("Byebye");
    }
}
