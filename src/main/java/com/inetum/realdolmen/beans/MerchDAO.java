package com.inetum.realdolmen.beans;

import com.inetum.realdolmen.entities.Hoodie;
import com.inetum.realdolmen.entities.Merch;
import com.inetum.realdolmen.entities.Mug;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class MerchDAO {

    @PersistenceContext(unitName = "InetumPU")
    private EntityManager entityManager;

    public List<Merch> getAll() {
        return entityManager.createQuery("select m from Merch m", Merch.class)
                            .getResultList();
    }

    public List<Merch> getMatchingByName(String filter) {
        if (filter == null || filter.isBlank()) {
            return getAll();
        } else {
            return entityManager.createQuery("select m from Merch m where lower(m.name) like ?1", Merch.class)
                                .setParameter(1, "%" + filter.trim() + "%")
                                .getResultList();
        }
    }

    public Hoodie findHoodieById(long id) {
        return entityManager.find(Hoodie.class, id);
    }

    public Mug findMugById(long id) {
        return entityManager.find(Mug.class, id);
    }

}
