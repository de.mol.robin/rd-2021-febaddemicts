package com.inetum.realdolmen;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/counter")
public class CounterServlet extends HttpServlet {

    private final String COUNTER = "COUNTER";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession ses = req.getSession();
        Object o = ses.getAttribute(COUNTER);
        if (o == null) {
            o = 0L;
            ses.setAttribute(COUNTER, o);
        }
        resp.getWriter().println(o);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader()
                         .lines()
                         .collect(Collectors.joining());
        var toAdd = Long.parseLong(body);
        var ses = req.getSession();
        Long c = (Long) ses.getAttribute(COUNTER);
        c += toAdd;
        ses.setAttribute(COUNTER, c);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute(COUNTER, 0L);
    }
}
