package com.inetum.realdolmen;

import com.inetum.realdolmen.beans.GuessingGameBean;
import com.inetum.realdolmen.beans.NumberParsingBean;
import com.inetum.realdolmen.exceptions.InvalidGuess;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/guess")
public class GuessingServlet extends HttpServlet {

    @EJB
    private NumberParsingBean numberParsingBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int remainingGuesses = getOrCreateGGBInSession(req.getSession()).getGuesses();
        resp.getWriter()
            .println(remainingGuesses);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int guess;
        try {
            guess = numberParsingBean.parseAsNumber(req.getParameter("g"));
            String outcome = getOrCreateGGBInSession(req.getSession()).makeGuess(guess);
            resp.getWriter()
                .println(outcome);
        } catch (InvalidGuess e) {
            resp.sendError(400, e.getMessage());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getOrCreateGGBInSession(req.getSession()).initializeGame();
    }

    private GuessingGameBean getOrCreateGGBInSession(HttpSession session) throws ServletException {
        GuessingGameBean ggb = (GuessingGameBean) session.getAttribute("GGB");
        if (ggb == null) {
            try {
                ggb = (GuessingGameBean) new InitialContext().lookup("java:module/GuessingGameBean");
                session.setAttribute("GGB", ggb);
            } catch (Exception e) {
                throw new ServletException("Could not obtain a game guessing bean instance!");
            }
        }
        return ggb;
    }

}
